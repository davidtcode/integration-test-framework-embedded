insert into users (username, password, enabled) values ('joe@bloggs.com', 'password', 1);
insert into roles (role_id, role) values (1, 'ROLE_USER');
insert into users_roles (username, role_id) values ('joe@bloggs.com', 1);

insert into system_colours (id, colour) values (1, 'red');
insert into system_colours (id, colour) values (2, 'green');
insert into system_colours (id, colour) values (3, 'blue');