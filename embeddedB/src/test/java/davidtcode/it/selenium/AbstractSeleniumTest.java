package davidtcode.it.selenium;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

/**
 * @author David.Tegart
 */
public abstract class AbstractSeleniumTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractSeleniumTest.class.getCanonicalName());

	public static final String IE = "ie";
	public static final String FIREFOX = "firefox";
	public static final String CHROME = "chrome";

	protected WebDriver driver;

	@Parameters({ "browserType", "url" })
	@BeforeTest()
	public void setUp(String browserType, String url) {

		LOG.debug("Testing url |{}| for browser |{}|", url, browserType);

		if (browserType.equalsIgnoreCase(FIREFOX)) {

			FirefoxProfile firefoxProfile = new FirefoxProfile();
			firefoxProfile.setAcceptUntrustedCertificates(true);

			driver = new FirefoxDriver(firefoxProfile);
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			
		} else if (browserType.equalsIgnoreCase(IE)) {
			// An IE driver can be added here
		} else if (browserType.equalsIgnoreCase(CHROME)) {
			// A Chrome driver can be added here
		} else {
			throw new IllegalArgumentException("The browser |" + browserType + "| is undefined");
		}

		setUpBeforeTest();
	}

	protected abstract void setUpBeforeTest();

	@AfterTest
	public void tearDown() {

		LOG.debug("Tearing down after base test class by closing driver");

		driver.close();

		tearDownAfterTest();
	}

	protected abstract void tearDownAfterTest();
}