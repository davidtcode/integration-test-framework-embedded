package davidtcode.it.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static davidtcode.it.selenium.SeleniumProperties.*;

/**
 * @author David.Tegart
 */
public class LoginPage extends AbstractBasePage {

	protected By usernameLocator = By.id(prop("login.css.username.id"));
	protected By passwordLocator = By.id(prop("login.css.password.id"));
	protected By loginButtonLocator = By.id(prop("login.css.login.id"));

	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		
		if (!prop("login.title").equals(driver.getTitle())) {
			throw new IllegalStateException("This is not the login page");
		}
	}

	public LoginPage typeUsername(String username) {

		driver.findElement(usernameLocator).sendKeys(username);

		return this;
	}

	public LoginPage typePassword(String password) {

		driver.findElement(passwordLocator).sendKeys(password);

		return this;
	}

	public LandingPage submitLogin() {

		driver.findElement(loginButtonLocator).submit();

		return new LandingPage(driver);
	}

	public LoginPage submitLoginExpectingFailure() {

		driver.findElement(loginButtonLocator).submit();

		return new LoginPage(driver);
	}

	public LandingPage loginAs(String username, String password) {

		typeUsername(username);
		typePassword(password);

		return submitLogin();
	}

	public String getUsernameFieldName() {
		return driver.findElement(usernameLocator).getAttribute("name");
	}

	public String getPasswordFieldName() {
		return driver.findElement(passwordLocator).getAttribute("name");
	}

	public boolean isLoginButtonPresent() {
		return isElementPresent(loginButtonLocator);
	}
}