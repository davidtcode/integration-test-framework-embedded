package davidtcode.it.selenium.pages;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static davidtcode.it.selenium.SeleniumProperties.*;

/**
 * @author David.Tegart
 */
public class LandingPage extends AbstractBasePage {

	private static final Logger LOG = LoggerFactory.getLogger(LandingPage.class
			.getCanonicalName());

	private By headerMessage = By.cssSelector(prop("landing.css.header.selector"));
	private By systemColours = By.cssSelector(prop("landing.css.colours.class"));
	private By systemColourContainer = By.id(prop("landing.css.colourContainer.id"));

	public LandingPage(WebDriver driver) {

		this.driver = driver;
		
		if (!prop("landing.title").equals(driver.getTitle())) {
			throw new IllegalStateException("This is not the landing page");
		}
	}

	public String getHeaderMessage() {
		return driver.findElement(headerMessage).getText();
	}

	public boolean isShowningSystemColours(Set<String> expectedSystemColours) {

		LOG.debug("The expected system colours are {}", expectedSystemColours);

		try {
			
			new WebDriverWait(driver, 5).until(
					new ExpectedCondition<Boolean>() {
						public Boolean apply(WebDriver d) {
							return d.findElement(systemColourContainer).getText().length() != 0;
						}
					});

			if(expectedSystemColours.size() != driver.findElements(this.systemColours).size()) {
				return false;
			}
			
			for(WebElement webElement : driver.findElements(this.systemColours)) {

				LOG.trace("Page has colour |{}|", webElement.getText());

				if(!expectedSystemColours.contains(webElement.getText().trim())){
					return false;
				}
			}

			return true;

		} catch (TimeoutException ex) {

			LOG.warn("Timed-out waiting for the landing page window title to change");

			return false;
		}
	}
}