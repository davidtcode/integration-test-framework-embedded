package davidtcode.it.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

/**
 * @author David.Tegart
 */
public abstract class AbstractBasePage {

	protected WebDriver driver;

	public String getWindowTitle() {
		return driver.getTitle();
	}

	protected boolean isElementPresent(By by) {
		
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException ex) {
			return false;
		}
	}
}