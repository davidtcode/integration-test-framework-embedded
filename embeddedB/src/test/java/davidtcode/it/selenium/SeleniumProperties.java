package davidtcode.it.selenium;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author David.Tegart
 */
public class SeleniumProperties {

	private static final Logger LOG = LoggerFactory
			.getLogger(SeleniumProperties.class.getCanonicalName());

	private static Properties properties = null;
	static {

		InputStream inputStream = null;
		try {

			inputStream = SeleniumProperties
					.class
					.getClassLoader()
					.getResourceAsStream("selenium.properties");

			properties = new Properties();
			properties.load(inputStream);

		} catch (Exception ex) {

			properties = null; // Let NPE fail any dependent tests
			LOG.error("The properties file cannot be loaded", ex);

		} finally {

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException ex) {
					LOG.error("Cannot close input stream", ex);
				}
			}
		}
	}

	public static String prop(String key) {
		return properties.getProperty(key);
	}
}