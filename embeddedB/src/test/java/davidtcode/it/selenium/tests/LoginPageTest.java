package davidtcode.it.selenium.tests;

import static org.testng.Assert.*;

import java.util.Arrays;
import java.util.HashSet;

import org.testng.annotations.Test;

import davidtcode.it.selenium.AbstractSeleniumTest;
import davidtcode.it.selenium.pages.LandingPage;
import davidtcode.it.selenium.pages.LoginPage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static davidtcode.it.selenium.SeleniumProperties.*;

/**
 * @author David.Tegart
 */
public class LoginPageTest extends AbstractSeleniumTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(LoginPageTest.class.getCanonicalName());

	protected LoginPage loginPage;
	protected LandingPage landingPage;

	@Test(priority = 1)
	public final void testLogin() {

		LOG.debug("Starting the test for the login page");

		// Firstly, hit the login page

		loginPage = new LoginPage(driver);

		checkLoginPage();

		// Then make a bad login attempt (no credentials entered!)

		loginPage = loginPage.submitLoginExpectingFailure();

		checkLoginPage();

		// And finally make a successful one

		landingPage = loginPage
				.typeUsername(prop("credential.username"))
				.typePassword(prop("credential.password"))
				.submitLogin();

		checkLandingPageAfterLogin();
	}

	protected final void checkLoginPage() {

		assertEquals(loginPage.getWindowTitle(), prop("login.title"));
		assertEquals(loginPage.getUsernameFieldName(), prop("login.field.username"));
		assertEquals(loginPage.getPasswordFieldName(), prop("login.field.password"));
		assertTrue(loginPage.isLoginButtonPresent());
	}

	protected final void checkLandingPageAfterLogin() {

		assertEquals(landingPage.getWindowTitle(), prop("landing.title"));
		assertEquals(landingPage.getHeaderMessage(), prop("landing.header.message"));
		assertTrue(landingPage.isShowningSystemColours(
				new HashSet<String>(
						Arrays.asList(
								prop("landing.colour1"), 
								prop("landing.colour2"), 
								prop("landing.colour3")))),
				"Can't find all the required system colours.");
	}
	
	protected void setUpBeforeTest() {}

	protected void tearDownAfterTest() {}
}