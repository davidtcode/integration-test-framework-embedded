### integration-test-framework-embedded

Written in late 2014, what follows is a skeleton example of how you might run automated system tests as part of a Maven build. More specifically, how you can deploy, during the build process, one or more web applications in an *embedded* Jetty container (alongside a H2 embedded database) and run Selenium tests against those applications.

The project consists of two applications. 

**EmbeddedA** is the web application which will be deployed and against which the Selenium tests are executed. It's pretty simple: it has a login page and a post-login landing page. The latter page makes an Ajax call, to a REST resource, to retrieve some made-up "system" colours and then displays them. 

**EmbeddedB** contains the actual Selenium tests. It also contains the configuration for the maven-jetty plugin which is responsible for, amongst other things, setting up the Jetty container as well as deploying the EmbeddedA web application. (See *embeddedB/pom.xml*.) The Selenium tests, located at *embeddedB/src/test/java/davidtcode/it/selenium/**, follow the "Page Object" pattern in the sense that each page is modeled as an object and tests (executed against that page) live elsewhere and are hence decoupled from those objects. The test framework used is TestNG.

Note that we could have done away with EmbeddedB and placed the tests, and the maven-jetty plugin, in EmbeddedA. The above approach is, I think, cleaner and makes more sense if we need to deploy more than one application.

### Prerequisites

You'll need Java 8, Maven 3+ and Firefox and its Selenium WebDriver. I'm using Firefox 40.0.3 but previous versions should work fine, just so long as the original install included the Selenium WebDriver, which it should have done. If it doesn't then it won't work. (See http://www.seleniumhq.org/download/ if you need to manually install the WebDriver.)

That should work for Windows. As your CI server is likely to run on some variety of Linux, and not have a display to launch a browser in, the Selenium tests will need to be executed *headlessly* or with a browser launched in virtual display. The easiest solution is to install and configure *Xvfb* (X-Virtual Frame Buffer); a little bit of research should show how this can be done. 

### How to run the tests?

* Clone the repository. 
* Change directory to base of the repository
* Run *mvn clean install*
    * This will build EmbeddedA and EmbeddedB, it will deploy the former to the embedded Jetty instance and then execute the Selenium tests. A Firefox window should open and then close.

By the way, the Jetty instance can be started, outside of the build, by doing the following:

* cd embeddedB/
* mvn jetty:run
* And the deployed embeddedA can be accessed at https://localhost:5443/embeddedA/
    * A self-signed certificate is being used, so you'll need to explicitly accept it.