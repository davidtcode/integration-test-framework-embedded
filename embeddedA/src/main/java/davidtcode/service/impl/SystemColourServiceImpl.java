package davidtcode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import davidtcode.dao.SystemColourDao;
import davidtcode.data.SystemColour;
import davidtcode.service.SystemColourService;

/**
 * @author David.Tegart
 */
@Service("systemColourService")
public class SystemColourServiceImpl implements SystemColourService {

	@Autowired
	protected SystemColourDao serviceColourDao;

	public List<SystemColour> getSystemColours() {

		return serviceColourDao.getSystemColours();
	}
}