package davidtcode.service;

import java.util.List;

import davidtcode.data.SystemColour;

/**
 * @author David.Tegart
 */
public interface SystemColourService {

	public List<SystemColour> getSystemColours();
}