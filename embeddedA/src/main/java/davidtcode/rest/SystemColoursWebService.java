package davidtcode.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import davidtcode.data.SystemColour;
import davidtcode.service.SystemColourService;

/**
 * @author David.Tegart
 */
@Path("/system")
public class SystemColoursWebService {

	@Autowired
	protected SystemColourService systemColourService;

	@GET
	@Path("/colour")
	@Produces(MediaType.APPLICATION_JSON)
	public List<SystemColour> getSystemColours() {

		return systemColourService.getSystemColours();
	}
}