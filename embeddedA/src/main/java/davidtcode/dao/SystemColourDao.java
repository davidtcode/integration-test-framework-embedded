package davidtcode.dao;

import java.util.List;

import davidtcode.data.SystemColour;

/**
 * @author David.Tegart
 */
public interface SystemColourDao {

	public List<SystemColour> getSystemColours();
}