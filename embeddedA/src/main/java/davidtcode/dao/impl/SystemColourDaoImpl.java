package davidtcode.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Repository;

import davidtcode.dao.SystemColourDao;
import davidtcode.data.SystemColour;

/**
 * @author David.Tegart
 */
@Repository("systemColourDao")
public class SystemColourDaoImpl implements SystemColourDao {

	@PersistenceContext()
	private EntityManager entityManager;

	public List<SystemColour> getSystemColours() {

		CriteriaQuery<SystemColour> criteria = getEntityManager()
				.getCriteriaBuilder().createQuery(SystemColour.class);

		criteria = criteria.select(criteria.from(SystemColour.class));

		return getEntityManager().createQuery(criteria).getResultList();
	}

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	protected void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}