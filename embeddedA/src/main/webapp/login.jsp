<%@ page language="java" contentType="text/html;charset=UTF-8"
	isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>EmbeddedA Login</title>

<style type="text/css">
body {font-serif;
	font-size: small;
}
</style>

</head>
<body>
	<c:if test='${not empty param.login_error}'>
		<span id="errorMessage" style="color: red; font-weight: bolder;">Your credentials are invalid</span>
	</c:if>
	<form method="POST" action="<c:url value="/j_spring_security_check"></c:url>">
		<table>
			<tr>
				<td align="right">Username</td>
				<td><input id="username" type="text" name="username"/></td>
			</tr>
			<tr>
				<td align="right">Password</td>
				<td><input id="password" type="password" name="password"/></td>
			</tr>
			<tr>
				<td colspan="2" align="right">
				<input type="submit" value="Login" id="login"/></td>
			</tr>
		</table>
	</form>
</body>
</html>